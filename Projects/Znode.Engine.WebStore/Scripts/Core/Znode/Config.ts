﻿module Config {
    export const PaymentScriptUrl = $("#hdnPaymentAppUrl").val() + "/script/znodeapijs";
    export const PaymentApplicationUrl = $("#hdnPaymentAppUrl").val() + "/";
    export const PaymentTwoCoUrl = $("#hdnWebStoreUrl").val() + "/orders/twoco?paymentSettingId=";
}