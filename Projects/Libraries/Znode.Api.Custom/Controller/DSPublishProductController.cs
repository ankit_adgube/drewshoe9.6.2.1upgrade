﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache.Cache;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Controller
{
    public class DSPublishProductController : BaseController
    {
        #region Private Variables

        private readonly IDSPublishProductCache _cache;
        private readonly IDSPublishProductService _service;

        #endregion

        #region Constructor
        public DSPublishProductController(IDSPublishProductService service)//:base(service)
        {
            _service = service;
            _cache = new DSPublishProductCache(_service);
        }
        #endregion

        [ResponseType(typeof(DSPublishProductResponse))]
        [HttpGet]
        public HttpResponseMessage GetStoreLocationsDetails(string state, string sku)
        {
            HttpResponseMessage response;


            try
            {
                DSPublishProductResponse data = _cache.GetStoreLocationDetails(state, sku);
                //var dt = data.StoreLocations;
                //string data1 = dt.ToString();
                response = (data != null) ? CreateOKResponse<DSPublishProductResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new DSPublishProductResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new DSPublishProductResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            System.Net.Http.Headers.HttpResponseHeaders h = response.Headers;
            return response;
        }
    }
}
