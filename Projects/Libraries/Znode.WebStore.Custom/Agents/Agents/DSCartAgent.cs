﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore.Agents;


using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSCartAgent : CartAgent
    {
        #region Private member       
        #endregion
        public DSCartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient, IAccountQuoteClient accountQuoteClient, IUserClient userClient) : base(shoppingCartsClient, publishProductClient, accountQuoteClient, userClient)
        {           
        }
        public override void GetSelectedGroupedProductsForAddToCart(AddToCartViewModel cartItem)
        {
            //Get sku's and quantity of associated group products.

            string[] groupProducts = string.IsNullOrEmpty(cartItem.GroupProductSKUs) ? cartItem.GroupProducts?.Select(x => x.Sku)?.ToArray() : cartItem.GroupProductSKUs?.Split(',');

            string[] groupProductsQuantity = string.IsNullOrEmpty(cartItem.GroupProductsQuantity) ? cartItem.GroupProducts?.Select(x => Convert.ToString(x.Quantity))?.ToArray() : cartItem.GroupProductsQuantity?.Split('_');

            //groupProducts[0] = "";
            cartItem.SKU = cartItem.SKU;
            cartItem.AddOnProductSKUs = cartItem.AddOnProductSKUs;
            cartItem.AutoAddonSKUs = cartItem.AutoAddonSKUs;

            for (int index = 0; index < groupProducts?.Length; index++)
            {
                bool isNewExtIdRequired = !Equals(index, 0);

                //cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                //cartItem.GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = groupProducts[index], Quantity = decimal.Parse(groupProductsQuantity[index]) } };

                ShoppingCartItemModel cartItemModel = BindConfigurableProducts(groupProducts[index], groupProductsQuantity[index], cartItem, isNewExtIdRequired);

                if (IsNotNull(cartItemModel))
                {
                    cartItem.ShoppingCartItems.Add(cartItemModel);
                }
            }
        }

        private ShoppingCartItemModel BindConfigurableProducts(string configurableSKU, string quantity, AddToCartViewModel cartItem, bool isNewExtIdRequired)
        {
            return new ShoppingCartItemModel
            {
                ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId,
                SKU = cartItem.SKU,
                ConfigurableProductSKUs = configurableSKU,
                AddOnProductSKUs = cartItem.AddOnProductSKUs,
                AutoAddonSKUs = cartItem.AutoAddonSKUs,
                Quantity = Convert.ToDecimal(quantity),
                PersonaliseValuesList = cartItem.PersonaliseValuesList,
                GroupProducts = new List<AssociatedProductModel>(),
                //IsProductEdit = cartItem.IsProductEdit
            };
        }


    }
}
